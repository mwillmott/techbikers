import React, { Component } from "react";
import DocumentTitle from "react-document-title";

class Sponsors extends Component {
  render() {
    return (
      <DocumentTitle title="Sponsors – Techbikers">
        <div className="content">
          <section>
            <header>
              <h1>Our sponsors</h1>
            </header>

            <div className="content">
              <p className="gap--big">We are honoured to be sponsored by the following companies, along with the effort of our riders they make Techbikers 2014 happen. We are incredibly grateful for their support, which means that all donations go directly to Room to Read.<br/></p>
                <h2 className="jersey--parent">Headline Sponsors</h2>

                <ul className="list--secret">
                  <li className="media">
                    <a href="http://www.google.com/entrepreneurs" className="img" target="_blank">
                      <img src="/static/img/sponsors/googleforentrepreneurs.png" alt="Google for Entrepreneurs" />
                    </a>
                    <div className="bd">
                      <h3 id="googleforentrepreneurs"><a href="http://www.google.com/entrepreneurs" target="_blank">Google for Entrepreneurs</a></h3>
                      <p>
                        Google for Entrepreneurs provides financial support and the best of Google's resources to dozens of coworking spaces and community programs across 125 countries. We also create Campuses: physical hubs where entrepreneurs can learn, connect, and build companies that will change the world.<br/>
                        <a href="http://plus.google.com/+GoogleForEntrepreneurs">+GoogleForEntrepreneurs</a> | <a href="http://www.twitter.com/GoogleForEntrep">@GoogleForEntrep</a>
                      </p>
                    </div>
                  </li>
                  <li className="media">
                    <a href="http://company.yandex.com/" className="img" target="_blank">
                      <img src="/static/img/sponsors/yandex_tight.png" alt="Yandex logo" />
                    </a>
                    <div className="bd">
                      <h3 id="yandex"><a href="http://company.yandex.com/" target="_blank">Yandex</a></h3>
                      <p>
                         Yandex is one of the largest internet companies in Europe, operating Russia’s most popular search engine and its most visited website. According to LiveInternet, as of December 2013, we generated 61.9% of all search traffic in Russia. We also operate in Ukraine, Kazakhstan, Belarus and Turkey.</p>
                      <p>Our mission is to help people solve their problems. Any problems. On- or offline, routine or one-off, personal or mathematical, geographical or logistical, theoretical or practical. For anyone. Anywhere. To that end, we develop and hone our search technologies and information retrieval services.<br/><a href="https://twitter.com/yandex">@Yandex</a> | <a href="https://www.facebook.com/yandex">facebook.com/yandex</a>
                      </p>
                    </div>
                  </li>
                </ul>

                <h2 className="jersey--parent">Core Sponsors</h2>

                <ul className="list--secret">
                  <li className="media">
                    <a href="http://tech.friedfrank.com" className="img" target="_blank">
                      <img src="/static/img/sponsors/friedfrank.png" alt="Fried Frank" />
                    </a>
                    <div className="bd">
                      <h3 id="friedfrank"><a href="http://tech.friedfrank.com" target="_blank">Fried Frank</a></h3>
                      <p>
                        Fried Frank is a leading New York-based international law firm with approximately 450 attorneys throughout the US, Europe and Asia.  Our London office, located in the heart of the Shoreditch tech cluster, works with some of the world's largest companies in their most significant transactions, as well as emerging companies seeking to expand to the US and access international markets.  We work with key contributors to the tech community – start-ups, accelerators, investors, multinational enterprises and government organizations – to provide US legal and business expertise in corporate structuring, financing, M&A, commercial contracts, IP, tax, employment, real estate and litigation.
                      </p>
                    </div>
                  </li>
                        <li className="media">
                    <a href="http://gettaxi.co.uk" className="img" target="_blank">
                      <img src="/static/img/sponsors/gettaxi.png" alt="GetTaxi" />
                          </a>
                    <div className="bd">
                      <h3 id="gettaxi"><a href="http://gettaxi.co.uk" target="_blank">GetTaxi</a></h3>
                      <p>
                        GetTaxi is London's number one free app for iPhone and Android that lets you book a licensed Black Taxi instantly with a tap of your mobile. GetTaxi is proud to be the only app in London that works exclusively with iconic Black Cabs, making us the most trusted & reliable taxi service in town.<br/>
                        <a href="https://twitter.com/GetTaxiUK">@GetTaxiUK</a> | <a href="https://www.facebook.com/GetTaxiUK">facebook.com/GetTaxiUK</a>
                      </p>
                          </div>
                  </li>
                </ul>

              <h2>Kit Sponsors</h2>
              <ul className="list--secret">
                        <li className="media">
                    <a href="https://shop.tribesports.co.uk/" className="img" target="_blank">
                      <img src="/static/img/sponsors/tribesports.jpg" alt="Tribesports" />
                    </a>
                    <div className="bd">
                      <h3 id="tribesports"><a href="https://shop.tribesports.co.uk/" target="_blank">Tribesports</a></h3>
                      <p>

            Tribesports is the world's first community-powered sportswear brand. Working with our community and the same level of designers, factories, fabrics and manufacturers as the world’s leading sports brands we create performance sportswear that exceeds expectation on quality and value.<br/><a href="https://twitter.com/Tribesports">@Tribesports</a> | <a href="https://www.facebook.com/Tribesports">facebook.com/Tribesports</a>
                      </p>
                          </div>
                  </li>
                </ul>

              <h2>Homecoming Party Sponsor</h2>
              <ul className="list--secret">
                        <li className="media">
                    <a href="http://www.mailjet.com/" className="img" target="_blank">
                      <img width="300px" src="/static/img/sponsors/mailjet.jpg" alt="Mailjet" />
                    </a>
                    <div className="bd">
                      <h3 id="tribesports"><a href="http://www.mailjet.com/" target="_blank">Mailjet</a></h3>
                      <p>
                        Mailjet is a powerful all-in-one email service provider that enables our customers to send transactional and marketing email to their contacts.<br/><a href="https://twitter.com/mailjet">@mailjet</a> | <a href="https://www.facebook.com/mailjet">facebook.com/mailjet</a>
                      </p>
                          </div>
                  </li>
                </ul>

                <h2 id="inkind">Sponsors in kind</h2>
                <p>The following sponsors have donated their products, time and energy to help TechBikers reach their goal, we are very thankful for their support</p>
                <ul className="list--secret list--inline inkind">
                  <li>
                    <a href="http://www.campuslondon.com/" className="img" target="_blank">
                      <img width="205" height="154" src="/static/img/sponsors/inkind/campus.png" alt="Campus London" />
                    </a>
                  </li>
                       <li>
                    <a style={{background: "#000"}} href="http://alexblogg.com/" className="img" target="_blank">
                      <img width="205" height="154" src="/static/img/sponsors/inkind/alexblogg.png" alt="Alex Blogg Video Production" />
                    </a>
                  </li>
                        <li>
                    <a href="http://believe.in/" className="img" target="_blank">
                      <img width="205" height="154" src="/static/img/sponsors/inkind/believein.png" alt="Believe.in" />
                    </a>
                  </li>
                  <li>
                    <a href="http://www.silvermansherliker.co.uk/" className="img" target="_blank">
                      <img width="205" height="154" src="/static/img/sponsors/inkind/SSLLP.png" alt="Silverman Sherliker LLP" />
                    </a>
                  </li>
                  <li>
                    <a href="http://www.rapha.cc/" className="img" target="_blank">
                      <img width="205" height="154" src="/static/img/sponsors/inkind/rapha.jpg" alt="Rapha" />
                    </a>
                  </li>
                  <li>
                    <a href="http://www.boommybody.com/" className="img" target="_blank">
                      <img style={{padding: "53px 0"}} width="205" src="/static/img/sponsors/inkind/boom.jpg" alt="BOOM CYCLE" />
                    </a>
                  </li>
                  <li>
                    <a href="http://meantimebrewing.com/" className="img" target="_blank">
                      <img style={{padding: "53px 0"}} width="205" src="/static/img/sponsors/inkind/meantime.png" alt="Meantime" />
                    </a>
                  </li>
                  <li>
                    <a href="http://www.smws.co.uk/" className="img" target="_blank">
                      <img width="205" src="/static/img/sponsors/inkind/smws_logo.jpg" alt="Scottish Malt Whisky Society" />
                    </a>
                  </li>
                </ul>
              </div>
          </section>
        </div>
      </DocumentTitle>
    );
  }
}

export default Sponsors;