import _ from "lodash";
import React, { Component } from "react";
import Marty from "marty";
import { FormattedNumber } from "react-intl";

import AuthContainer from "./authContainer.jsx";
import PaymentForm from "./paymentForm.jsx";

class RideRegistrationForm extends Component {

  static propTypes = {
    ride: React.PropTypes.object.isRequired,
    currentRider: React.PropTypes.object.isRequired
  }

  // Register the current user for the ride
  register(card) {
    let { ride, currentRider } = this.props;

    Stripe.setPublishableKey(ride.chapter.public_key);
    Stripe.card.createToken(card, (status, res) => {
      if (res.error) {
        console.log('stripe error: ' + res.error)
      } else {
        this.app.rideActions.registerRider(ride, currentRider, res.id);
      }
    });
  }

  render() {
    let { ride, currentRider } = this.props;
    let submitText = (
      <span>Pay <FormattedNumber style="currency" currency={ride.chapter.currency} value={ride.chapter.membership_fee} /> & Register Interest</span>
    );

    return (
      <div className="ride-registration--form">
        <div className="ride-registration--details">
          <ol className="steps">
            <li className="active">
              <span>1</span>
              <span>Join & Register</span>
            </li>

            <li>
              <span>2</span>
              <span>Riders Invited</span>
            </li>

            <li>
              <span>3</span>
              <span>Confirm & Pay</span>
            </li>
          </ol>
          <h3>Techbikers: 1 Year Membership (<FormattedNumber style="currency" currency={ride.chapter.currency} value={ride.chapter.membership_fee} />)</h3>
          <p>
            Joining Techbikers gives you access to our events and the community, allows you to join us for training rides (even if your not coming on the main
            ride!) and helps support an amazing cause.</p>
          <p>
            Joining and registering your interest does not guarantee a spot on the ride. We ask people to join & pay a <strong>non-refundable membership
            fee</strong> so we know who is serious.</p>
        </div>
        <PaymentForm submitText={submitText} onSubmit={this.register} />
      </div>
    );
  }
}

export default Marty.createContainer(RideRegistrationForm);