import Marty, { ActionCreators } from "marty";
import ActionConstants from "../constants/actionConstants";

class RideActions extends ActionCreators {

  registerRider(ride, rider, token) {
    this.dispatch(ActionConstants.CREATE_RIDE_REGISTRATION_STARTING, ride, rider);
    this.dispatch(ActionConstants.CREATE_CHAPTER_MEMBERSHIP_STARTING, ride.chapter, rider);

    // First join the rider to the chapter and charge them
    this.app.chapterAPI.join(ride.chapter.id, token).then(
      result => {
        this.dispatch(ActionConstants.CREATE_CHAPTER_MEMBERSHIP, ride.chapter, rider);

        // Now register the user for the ride (this will register them in
        // the pending state so no charge is made to their card)
        this.app.rideAPI.registerRider(ride.id, rider.id).then(
          result => {
            this.dispatch(ActionConstants.CREATE_RIDE_REGISTRATION, ride.id, result);
          },
          error => {
            this.dispatch(ActionConstants.CREATE_RIDE_REGISTRATION_FAILED, ride, rider, error);
          });
      },
      error => {
        this.dispatch(ActionConstants.CREATE_CHAPTER_MEMBERSHIP, ride.chapter, rider, error);
        this.dispatch(ActionConstants.CREATE_RIDE_REGISTRATION_FAILED, ride, rider, error);
      });
  }
}

export default RideActions;